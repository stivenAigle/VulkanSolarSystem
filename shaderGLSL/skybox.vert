#version 450
#extension GL_ARB_separate_shader_objects : enable


layout(location = 0) in  vec3 position;
layout (location = 0) out vec3 uv;
layout(location = 1)out vec3 posViewSpace;

out gl_PerVertex {
    vec4 gl_Position;
};


layout(set = 0, binding = 0) uniform camera{
    mat4 proj;
    mat4 view;
}cam;


void main() {

    mat4 cal = cam.view;
    gl_Position = cam.proj* mat4(mat3(cal)) * vec4(position, 1) ;
    uv = position;
    uv.y = -uv.y;
    posViewSpace = gl_Position.xyz;

}
