#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) out vec4 outColor;

layout(location = 0) in vec3 uv;
layout(location = 1)in vec3 posViewSpace;
layout(set = 0, binding = 1) uniform samplerCube text;

void main() {

    outColor = texture(text, uv);
//    outColor = vec4(0.2, 0, 0, 1);
    float coefFog = 1 - float(posViewSpace.z)/2.0f;
    vec4 colorFog = vec4(0.7, 0.7, 0.7, 1);
    outColor = coefFog*outColor + (1-coefFog)*colorFog;
}
