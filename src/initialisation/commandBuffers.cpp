//
// Created by stiven on 17-11-21.
//

#include <uniformBuffer/chooseObject.hpp>
#include "commandBuffers.hpp"
#include "renderPasses.hpp"
#include "descriptorSet.hpp"

CommandBuffers::CommandBuffers(SwapChain &swapChain, CommandPool &commandPool, LogicalDevice &logicalDevice,
                               RenderPasses &renderPasses, FrameBuffers &frameBuffers,
                               GraphicsPipeline &graphicsPipeline, Vertex &vertices, DescriptorSet &descriptorSet,
                               UniformBuffer &uniform)
		: swapChain(swapChain),
		  commandPool(commandPool) ,
logicalDevice(logicalDevice),
renderPasses(renderPasses),
frameBuffers(frameBuffers),
graphicsPipeline(graphicsPipeline), vertices(vertices),
descriptorSet(descriptorSet),
uniform(uniform){

	commandBuffers.resize(swapChain.getSwapChainImageViews().size());
	VkCommandBufferAllocateInfo allocateInfo{};
	allocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	allocateInfo.commandPool = commandPool;
	allocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	allocateInfo.commandBufferCount = static_cast<uint32_t>(commandBuffers.size());




	if (vkAllocateCommandBuffers(logicalDevice, &allocateInfo, commandBuffers.data()) != VK_SUCCESS) {
		throw std::runtime_error("failed to allocate command buffers!");
	}

	secondaryCommandBuffers.resize(swapChain.getSwapChainImageViews().size());
	VkCommandBufferAllocateInfo infoSecondary{};
	infoSecondary.commandPool = commandPool;
	infoSecondary.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	infoSecondary.commandBufferCount = static_cast<uint32_t>(secondaryCommandBuffers.size());
	infoSecondary.level = VK_COMMAND_BUFFER_LEVEL_SECONDARY;

	if (vkAllocateCommandBuffers(logicalDevice, &infoSecondary, secondaryCommandBuffers.data()) != VK_SUCCESS) {
		throw std::runtime_error("failed to allocate command buffers!");
	}
	recordSecondary();
	record(graphicsPipeline);

}

void CommandBuffers::recordSecondary() {
	for(unsigned i = 0; i < secondaryCommandBuffers.size(); i++) {
		VkCommandBufferBeginInfo begin{};
		begin.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		begin.flags = VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT | VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;
		VkCommandBufferInheritanceInfo inheritanceInfo{};

		inheritanceInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO;
		inheritanceInfo.renderPass = renderPasses;
		inheritanceInfo.subpass = 1;
		inheritanceInfo.framebuffer = frameBuffers[i];


		begin.pInheritanceInfo = &inheritanceInfo;
		vkBeginCommandBuffer(secondaryCommandBuffers[i], &begin);

		vkCmdBindPipeline(secondaryCommandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, graphicsPipeline);
		VkBuffer vertex[]{vertices.getBoxBuffer()};
		VkDeviceSize offset[]{0};
		VkDescriptorSet a = descriptorSet;
		object change{1};
		uniform.getObject().update(change);
		vkCmdBindDescriptorSets(secondaryCommandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, graphicsPipeline.getPipelineLayout(), 0, 1, &a, 0, nullptr);
		vkCmdBindVertexBuffers(secondaryCommandBuffers[i], 0, 1, vertex, offset);
		vkCmdDraw(secondaryCommandBuffers[i], 36, 1, 0, 0);

		vkEndCommandBuffer(secondaryCommandBuffers[i]);


	}

}

void CommandBuffers::record(GraphicsPipeline &pipeline) {

	for(unsigned i = 0; i<commandBuffers.size(); i++){
		//init !
		VkCommandBufferBeginInfo begin{};
		begin.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		begin.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;

		vkBeginCommandBuffer(commandBuffers[i], &begin) ;

		VkRenderPassBeginInfo passBeginInfo{};
		passBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
		passBeginInfo.renderPass = renderPasses;
		passBeginInfo.framebuffer = frameBuffers[i];
		passBeginInfo.renderArea.offset = {0, 0};
		passBeginInfo.renderArea.extent= swapChain.getSwapChainExtent() ;
		std::array<VkClearValue, 2> clearValues = {};
		clearValues[0].color = {0.5f, 0.5f, 0.5f, 1.0f};
		clearValues[1].depthStencil = {1.0f, 0};

		passBeginInfo.clearValueCount = 2;
		passBeginInfo.pClearValues = clearValues.data();
		VkBuffer vertex[]{vertices, vertices.getSkyBoxBuffer(), vertices.getBoxBuffer()};
		VkDeviceSize offset[]{0};

		//Begin record
		vkCmdBeginRenderPass(commandBuffers[i], &passBeginInfo, VK_SUBPASS_CONTENTS_INLINE);
		vkCmdBindPipeline(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, graphicsPipeline.getSkyBoxPipeline());
		vkCmdBindVertexBuffers(commandBuffers[i], 0, 1, &vertex[1], offset);

		VkDescriptorSet  skyBox = descriptorSet.getSkyBoxDescriptor();
		vkCmdBindDescriptorSets(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline.getSkyBoxLayout(), 0, 1, &skyBox, 0, nullptr);
		vkCmdDraw(commandBuffers[i], 36, 1, 0, 0);

		object change{0};
		uniform.getObject().update(change);

		vkCmdNextSubpass(commandBuffers[i], VK_SUBPASS_CONTENTS_INLINE);
		VkDescriptorSet a = descriptorSet;
		int test = 1;
		vkCmdBindPipeline(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, graphicsPipeline);

		vkCmdPushConstants(commandBuffers[i], pipeline.getPipelineLayout(), VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(int), &test);

		vkCmdBindVertexBuffers(commandBuffers[i], 0, 1, &vertex[2], offset);
//		uniform.getObject().update(change);
		vkCmdBindDescriptorSets(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline.getPipelineLayout(), 0, 1, &a, 0, nullptr);
		vkCmdDraw(commandBuffers[i], 36, 1, 0, 0);


		vkCmdBindVertexBuffers(commandBuffers[i], 0, 1, vertex, offset);


		vkCmdBindPipeline(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, graphicsPipeline.getStencilPipeline());
		test = 3;
		vkCmdPushConstants(commandBuffers[i], pipeline.getPipelineLayout(), VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(int), &test);
		vkCmdDraw(commandBuffers[i], vertices.getSize(), Scene::numberPlanet, 0, 0);
		test = 2;
		vkCmdPushConstants(commandBuffers[i], pipeline.getPipelineLayout(), VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(int), &test);
		vkCmdDraw(commandBuffers[i], vertices.getSize(), Scene::numberPlanet, 0, 0);

		vkCmdBindPipeline(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, graphicsPipeline );

		change.ob = 0;
		uniform.getObject().update(change);
		vkCmdBindDescriptorSets(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline.getPipelineLayout(), 0, 1, &a, 0, nullptr);
		test = 0;
		vkCmdPushConstants(commandBuffers[i], pipeline.getPipelineLayout(), VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(int), &test);
		vkCmdDraw(commandBuffers[i], vertices.getSize(), Scene::numberPlanet, 0, 0);

		//vkCmdExecuteCommands(commandBuffers[i], 1, &secondaryCommandBuffers[i]);
		vkCmdEndRenderPass(commandBuffers[i]);
		if (vkEndCommandBuffer(commandBuffers[i]) != VK_SUCCESS) {
			throw std::runtime_error("failed to record command planetBuffer!");
		}

	}
}
