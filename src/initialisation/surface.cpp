//
// Created by stiven on 17-11-20.
//

#include "surface.hpp"

Surface::Surface(Instance &instance, Window &window) :instance(instance){

	if (glfwCreateWindowSurface(instance, window, nullptr, &surface) != VK_SUCCESS) {
		throw std::runtime_error("failed to create window surface!");
	}
}

Surface::~Surface() {
	vkDestroySurfaceKHR(instance, surface, nullptr);
}
