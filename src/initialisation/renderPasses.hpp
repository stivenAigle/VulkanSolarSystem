//
// Created by stiven on 17-11-21.
//

#ifndef SOLARSYSTEM_RENDERPASSES_HPP
#define SOLARSYSTEM_RENDERPASSES_HPP


#include "swapChain.hpp"

class RenderPasses {
	SwapChain & swapChain;
	LogicalDevice & logicalDevice;
	PhysicalDevice & physicalDevice;
	VkRenderPass  renderPass;


public:
	RenderPasses(SwapChain &swapChain, LogicalDevice &logicalDevice, PhysicalDevice &physicalDevice);

	~RenderPasses();


	operator VkRenderPass (){ return renderPass;}
};


#endif //SOLARSYSTEM_RENDERPASSES_HPP
