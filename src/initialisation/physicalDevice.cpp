//
// Created by stiven on 17-11-20.
//

#include <vector>
#include <set>
#include <iostream>
#include "physicalDevice.hpp"
#include "surface.hpp"
#include "swapChain.hpp"
const std::vector<const char*> deviceExtensions {
		VK_KHR_SWAPCHAIN_EXTENSION_NAME
};

bool PhysicalDevice::checkDeviceExtensionSupport(VkPhysicalDevice physicalDevice) {
	uint32_t count;
	vkEnumerateDeviceExtensionProperties(physicalDevice, nullptr, &count, nullptr);
	std::vector<VkExtensionProperties> properties(count);
	vkEnumerateDeviceExtensionProperties(physicalDevice, nullptr, &count, properties.data());

	std::set<std::string> required(std::begin(deviceExtensions), std::end(deviceExtensions));

	for (const auto &extension : properties) {
		required.erase(extension.extensionName);
	}

	return required.empty();

}

swapChainSupportDetail PhysicalDevice::querySwapChainSupport(VkPhysicalDevice device) {
	swapChainSupportDetail details;
	// surface capabilities min/max number of image
	vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface, &details.capabilities);

	// surface format available pixel and color space
	uint32_t countFormat;
	vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &countFormat, nullptr);
	if (countFormat != 0) {
		details.formats.resize(countFormat);
		vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &countFormat, details.formats.data());
	}

	// presentaton mode
	uint32_t presentModeCount;
	vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount, nullptr);

	if (presentModeCount != 0) {
		details.presentModes.resize(presentModeCount);
		vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount, details.presentModes.data());
	}

	return details;
}

bool PhysicalDevice::isDeviceSuitable(VkPhysicalDevice device) {

	queueFamilyIndices indices = findQueueFamilies(device);

	bool extensionSupported = checkDeviceExtensionSupport(device);
	VkPhysicalDeviceFeatures supportedFeatures;
	vkGetPhysicalDeviceFeatures(device, &supportedFeatures);
	bool swapChainAdequate = false;
	if(extensionSupported){
		swapChainSupportDetail supportDetail = querySwapChainSupport(device);
		swapChainAdequate = !supportDetail.formats.empty() && !supportDetail.presentModes.empty();
	}
	return indices.isComplete() && extensionSupported && swapChainAdequate && supportedFeatures.samplerAnisotropy;
}

// On regarde pour deux queues, la graphics queue pour les calcules et la present queue pour le rendu a l'ecran
queueFamilyIndices PhysicalDevice::findQueueFamilies(VkPhysicalDevice device) {
	queueFamilyIndices indices;
	uint32_t queueFamilyCount = 0;
	vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, nullptr);
	std::vector<VkQueueFamilyProperties> queuesFamilies(queueFamilyCount);
	vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, queuesFamilies.data());
	int i = 0;
	VkBool32 presentSupport = VK_FALSE;
	for (auto &queueFamily : queuesFamilies) {
		if (queueFamily.queueCount > 0 && queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT) {
			indices.graphicsFamily = i;
		}
		presentSupport = VK_FALSE;
		vkGetPhysicalDeviceSurfaceSupportKHR(device, i, surface, &presentSupport);
		if (queueFamily.queueCount > 0 && presentSupport) {
			indices.presentFamily = i;
		}

		if (indices.isComplete()) {
			break;
		}
		i++;


	}
	return indices;
}

void PhysicalDevice::choosePhysicalDevice() {
	uint32_t count;
	vkEnumeratePhysicalDevices(instance, &count, nullptr);
	std::vector<VkPhysicalDevice> devices(count);
	vkEnumeratePhysicalDevices(instance, &count, devices.data());

	for (auto physDevice : devices) {
		if (isDeviceSuitable(physDevice)) {
			device = physDevice;
			indices = findQueueFamilies(physDevice);
			break;
		}
	}
}

PhysicalDevice::~PhysicalDevice() {}

PhysicalDevice::PhysicalDevice(Instance &instance, Surface &surface) : instance(instance), surface(surface) {
	choosePhysicalDevice();
	VkPhysicalDeviceProperties a;
	vkGetPhysicalDeviceProperties(device, &a);

}

const queueFamilyIndices &PhysicalDevice::getQueueFamilyIndices() const {
	return indices;
}

int PhysicalDevice::getGraphicsQueue() const {
	return indices.graphicsFamily;
}

int PhysicalDevice::getPresentQueue() const {
	return indices.presentFamily;

}


