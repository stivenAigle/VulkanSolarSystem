//
// Created by stiven on 17-11-20.
//

#ifndef SOLARSYSTEM_PHYSICALDEVICE_HPP
#define SOLARSYSTEM_PHYSICALDEVICE_HPP


#include <vulkan/vulkan.h>
#include "instance.hpp"
#include "surface.hpp"
#include <vector>

struct swapChainSupportDetail;

struct queueFamilyIndices{
	int graphicsFamily = -1;
	int presentFamily = -1;
	bool isComplete(){
			return graphicsFamily != -1 && presentFamily != -1;
	};
};
extern const std::vector<const char*> deviceExtensions ;
class PhysicalDevice {

	VkPhysicalDevice device;
	Instance& instance;
	Surface & surface;

	queueFamilyIndices indices;
	void choosePhysicalDevice();
public:
	PhysicalDevice(Instance &instance, Surface &surface);
	operator VkPhysicalDevice (){ return device;}
	~PhysicalDevice();

	queueFamilyIndices findQueueFamilies(VkPhysicalDevice device);

	const queueFamilyIndices &getQueueFamilyIndices() const;
	int getGraphicsQueue() const;
	int getPresentQueue() const;

	bool isDeviceSuitable(VkPhysicalDevice device);

	bool checkDeviceExtensionSupport(VkPhysicalDevice physicalDevice);

	swapChainSupportDetail querySwapChainSupport(VkPhysicalDevice device);

};


#endif //SOLARSYSTEM_PHYSICALDEVICE_HPP
