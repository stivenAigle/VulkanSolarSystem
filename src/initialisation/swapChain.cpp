//
// Created by stiven on 17-11-20.
//

#include "swapChain.hpp"
SwapChain::SwapChain(LogicalDevice &logicalDevice, PhysicalDevice &physicalDevice, Surface &surface)
		: logicalDevice(logicalDevice),
		  physicalDevice(physicalDevice) {
	detail = physicalDevice.querySwapChainSupport(physicalDevice);

	VkSurfaceFormatKHR surfaceFormat = chooseSwapChainSurfaceFormat();
	VkPresentModeKHR presentMode = chooseSwapPresentMode();
	VkExtent2D extent = chooseSwapExtent();



	uint32_t imageCount = detail.capabilities.minImageCount + 1;
	if(detail.capabilities.maxImageCount > 0 && imageCount >detail.capabilities.maxImageCount){
		imageCount = detail.capabilities.maxImageCount;
	}

	VkSwapchainCreateInfoKHR swapChainInfo{};
	swapChainInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	swapChainInfo.surface = surface;
	swapChainInfo.minImageCount = imageCount;
	swapChainInfo.imageFormat = surfaceFormat.format;
	swapChainInfo.imageExtent = extent;
	swapChainInfo.imageColorSpace = surfaceFormat.colorSpace;
	swapChainInfo.imageArrayLayers = 1;
	swapChainInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

	// multiple queue
	uint32_t queueFamilyIndices[] {static_cast<uint32_t>(physicalDevice.getGraphicsQueue()),
	                               static_cast<uint32_t>(physicalDevice.getPresentQueue())};

	if (physicalDevice.getGraphicsQueue() != physicalDevice.getPresentQueue()) {
		swapChainInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
		swapChainInfo.queueFamilyIndexCount = 2;
		swapChainInfo.pQueueFamilyIndices = queueFamilyIndices;
	} else {
		swapChainInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
		swapChainInfo.queueFamilyIndexCount = 0; // Optional
		swapChainInfo.pQueueFamilyIndices = nullptr; // Optional
	}

	swapChainInfo.preTransform = detail.capabilities.currentTransform;
	swapChainInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
	swapChainInfo.presentMode = presentMode;
	swapChainInfo.clipped = VK_TRUE;
	swapChainInfo.oldSwapchain = VK_NULL_HANDLE;

	if(vkCreateSwapchainKHR(logicalDevice, &swapChainInfo, nullptr, &swapChain)){
		throw std::runtime_error("failed to create swapchain");
	}
	vkGetSwapchainImagesKHR(logicalDevice, swapChain, &imageCount, nullptr);
	swapChainImage.resize(imageCount);
	vkGetSwapchainImagesKHR(logicalDevice, swapChain, &imageCount, swapChainImage.data());

	swapChainExtent = extent;
	swapChainFormat = surfaceFormat.format;

	createImageView();
}


VkSurfaceFormatKHR SwapChain::chooseSwapChainSurfaceFormat() {
	std::vector<VkSurfaceFormatKHR> & format = detail.formats;

	//la swap chain n'as pas format prefere, on prend ce que l'on veut
	if(format.size() == 1 && format[0].format == VK_FORMAT_UNDEFINED){
		return {VK_FORMAT_B8G8R8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR};
	}

	for (const auto & formatAvailable : format){
		if (formatAvailable.format == VK_FORMAT_B8G8R8A8_UNORM && formatAvailable.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
			return formatAvailable;
		}
	}
	return format[0];
}

VkPresentModeKHR SwapChain::chooseSwapPresentMode() {

	VkPresentModeKHR bestMode = VK_PRESENT_MODE_FIFO_KHR;

	for (const auto& availablePresentMode : detail.presentModes) {
		if (availablePresentMode == VK_PRESENT_MODE_MAILBOX_KHR) {
			return availablePresentMode;
		} else if (availablePresentMode == VK_PRESENT_MODE_IMMEDIATE_KHR) {
			bestMode = availablePresentMode;
		}
	}

	return bestMode;
}

VkExtent2D SwapChain::chooseSwapExtent() {
	VkSurfaceCapabilitiesKHR & capabilities = detail.capabilities;

	if(capabilities.currentExtent.width != std::numeric_limits<uint32_t>::max()){
		return capabilities.currentExtent;
	}else{
		VkExtent2D actualExtent{static_cast<uint32_t>(Window::getWidth()), static_cast<uint32_t>(Window::getHeigth())};

		actualExtent.width = std::max(capabilities.minImageExtent.width, std::max(capabilities.maxImageExtent.width, actualExtent.width));
		actualExtent.height = std::max(capabilities.minImageExtent.height, std::min(capabilities.maxImageExtent.height, actualExtent.height));

		return actualExtent;
	}
}

void SwapChain::createImageView() {
	swapChainImageViews.resize(swapChainImage.size());
	for (uint32_t i = 0; i < swapChainImageViews.size() ; i++){
		VkImageViewCreateInfo createInfo{};
		createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		createInfo.format = swapChainFormat;
		createInfo.image = swapChainImage[i];
		createInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
		createInfo.components.a = VK_COMPONENT_SWIZZLE_A;
		createInfo.components.b = VK_COMPONENT_SWIZZLE_B;
		createInfo.components.r = VK_COMPONENT_SWIZZLE_R;
		createInfo.components.g = VK_COMPONENT_SWIZZLE_G;
		createInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		createInfo.subresourceRange.baseMipLevel = 0;
		createInfo.subresourceRange.levelCount = 1;
		createInfo.subresourceRange.baseArrayLayer = 0;
		createInfo.subresourceRange.layerCount = 1;

		if(vkCreateImageView(logicalDevice, &createInfo, nullptr, &swapChainImageViews[i])){
			throw std::runtime_error("error to create image view");
		}
	}
}

SwapChain::~SwapChain() {

	vkDestroySwapchainKHR(logicalDevice, swapChain, nullptr);
	for(auto & imageView: swapChainImageViews){
		vkDestroyImageView(logicalDevice, imageView, nullptr);
	}
}

const VkExtent2D &SwapChain::getSwapChainExtent() const {
	return swapChainExtent;
}

VkFormat SwapChain::getSwapChainFormat() const {
	return swapChainFormat;
}

const std::vector<VkImageView> &SwapChain::getSwapChainImageViews() const {
	return swapChainImageViews;
}
