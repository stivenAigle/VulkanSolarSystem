//
// Created by stiven on 17-11-20.
//

#ifndef SOLARSYSTEM_LOGICALDEVICE_HPP
#define SOLARSYSTEM_LOGICALDEVICE_HPP


#include <vulkan/vulkan.h>
#include "physicalDevice.hpp"
#include <vector>
class LogicalDevice {
	VkDevice device;
	VkQueue graphicsQueue;
	VkQueue presentQueue;

	PhysicalDevice &physicalDevice;

public:

	LogicalDevice(PhysicalDevice &physicalDevice) ;

	operator VkDevice (){ return device;}

	VkQueue getGraphicsQueue() ;

	VkQueue getPresentQueue() ;
	void destroy();
	~LogicalDevice();
};


#endif //SOLARSYSTEM_LOGICALDEVICE_HPP
