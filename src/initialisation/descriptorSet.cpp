//
// Created by stiven on 17-12-01.
//

#include <iostream>
#include <uniformBuffer/position.hpp>
#include <texture/PlanetsTexture.hpp>
#include <uniformBuffer/projView.hpp>
#include <texture/skyBox.hpp>
#include <uniformBuffer/uniform.hpp>
#include <uniformBuffer/chooseObject.hpp>
#include "descriptorSet.hpp"
#include "descriptorSetLayout.hpp"

DescriptorSet::DescriptorSet(LogicalDevice &logicalDevice, DescriptorPool &descriptorPool,
                             DescriptorSetLayout &descriptorSetLayout, UniformBuffer &uni, PlanetsTexture &textures,
                             SkyBox &skybox, Uniform <object> &ob) : logicalDevice(
		logicalDevice), descriptorPool(descriptorPool), descriptorSetLayout(descriptorSetLayout), uniformBuffer(uni) {

	VkDescriptorSetLayout setLayouts[]{descriptorSetLayout};
	VkDescriptorSetAllocateInfo allocateInfo{};
	allocateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	allocateInfo.descriptorPool = descriptorPool;
	allocateInfo.descriptorSetCount = 1;
	allocateInfo.pSetLayouts = setLayouts;

	if(vkAllocateDescriptorSets(logicalDevice, &allocateInfo, &descriptorSet)){
		throw std::runtime_error{"failed to create descriptor set"};
	}

	std::array<VkDescriptorBufferInfo, Scene::numberPlanet*2> bufferInfos{};
	for(unsigned i = 0; i< Scene::numberPlanet; i++) {
		bufferInfos[i].buffer = uni[i];
		bufferInfos[i].offset = 0;
		bufferInfos[i].range = sizeof(transform);
	}

	for(unsigned i = Scene::numberPlanet; i< Scene::numberPlanet*2; i++) {
		bufferInfos[i].buffer = uni.getPosUni(i-Scene::numberPlanet);
		bufferInfos[i].offset = 0;
		bufferInfos[i].range = sizeof(pos);
	}

	std::array<VkDescriptorImageInfo, Scene::numberPlanet> imageInfos{};
	for(unsigned i = 0; i < imageInfos.size(); i++ ){
		imageInfos[i].imageView = textures[i];
		imageInfos[i].imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		imageInfos[i].sampler = textures[i].getSampler();
	}

	VkDescriptorImageInfo cubeInfo{};
	cubeInfo.imageView = skybox.getImageView();
	cubeInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
	cubeInfo.sampler = skybox.getSampler();

	VkDescriptorBufferInfo obInfo{};
	obInfo.buffer = ob;
	obInfo.offset = 0;
	obInfo.range = ob.size;

	VkWriteDescriptorSet writeDescriptorSet{};
	writeDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	writeDescriptorSet.dstSet = descriptorSet;
	writeDescriptorSet.descriptorCount = Scene::numberPlanet;
	writeDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	writeDescriptorSet.dstBinding = 0;
	writeDescriptorSet.dstArrayElement = 0;
	writeDescriptorSet.pBufferInfo = bufferInfos.data();

	VkWriteDescriptorSet writeDescriptorSetBind1{};
	writeDescriptorSetBind1.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	writeDescriptorSetBind1.dstSet = descriptorSet;
	writeDescriptorSetBind1.descriptorCount = Scene::numberPlanet;
	writeDescriptorSetBind1.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	writeDescriptorSetBind1.dstBinding = 1;
	writeDescriptorSetBind1.dstArrayElement = 0;
	writeDescriptorSetBind1.pBufferInfo = &bufferInfos[Scene::numberPlanet];

	VkWriteDescriptorSet writeDescriptorSetBindImage{};
	writeDescriptorSetBindImage.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	writeDescriptorSetBindImage.dstSet = descriptorSet;
	writeDescriptorSetBindImage.descriptorCount = Scene::numberPlanet;
	writeDescriptorSetBindImage.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	writeDescriptorSetBindImage.dstBinding = 2;
	writeDescriptorSetBindImage.dstArrayElement = 0;
	writeDescriptorSetBindImage.pImageInfo = imageInfos.data();

	VkWriteDescriptorSet writeDescriptorSetcube{};
	writeDescriptorSetcube.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	writeDescriptorSetcube.dstSet = descriptorSet;
	writeDescriptorSetcube.descriptorCount = 1;
	writeDescriptorSetcube.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	writeDescriptorSetcube.dstBinding = 3;
	writeDescriptorSetcube.dstArrayElement = 0;
	writeDescriptorSetcube.pImageInfo = &cubeInfo;

	VkWriteDescriptorSet writeDescriptorSetBind4{};
	writeDescriptorSetBind4.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	writeDescriptorSetBind4.dstSet = descriptorSet;
	writeDescriptorSetBind4.descriptorCount = 1;
	writeDescriptorSetBind4.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	writeDescriptorSetBind4.dstBinding = 4;
	writeDescriptorSetBind4.dstArrayElement = 0;
	writeDescriptorSetBind4.pBufferInfo = &obInfo;

	VkWriteDescriptorSet writes[]{writeDescriptorSet, writeDescriptorSetBind1, writeDescriptorSetBindImage, writeDescriptorSetcube, writeDescriptorSetBind4};

	vkUpdateDescriptorSets(logicalDevice, 5, writes, 0, nullptr);
	createSkyBoxDescriptor(skybox);
}

VkDescriptorSet DescriptorSet::getSkyBoxDescriptor() const {
	return skyBoxDescriptor;
}

void DescriptorSet::createSkyBoxDescriptor(SkyBox &skyboox) {

	VkDescriptorSetAllocateInfo allocateInfo{};
	allocateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	allocateInfo.descriptorPool = descriptorPool;
	allocateInfo.descriptorSetCount = 1;
	allocateInfo.pSetLayouts = descriptorSetLayout.geDescCam();

	if(vkAllocateDescriptorSets(logicalDevice, &allocateInfo, &skyBoxDescriptor)){
		throw std::runtime_error{"failed to create descriptor set"};
	}


	VkDescriptorBufferInfo bufferInfo{};
	bufferInfo.buffer = uniformBuffer.getUniCam();
	bufferInfo.range = sizeof(projView);
	bufferInfo.offset = 0;

	VkDescriptorImageInfo imageInfo{};
	imageInfo.imageView = skyboox.getImageView();
	imageInfo.sampler = skyboox.getSampler();
	imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;



	VkWriteDescriptorSet writeDescriptorSetBind1{};
	writeDescriptorSetBind1.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	writeDescriptorSetBind1.dstSet = skyBoxDescriptor;
	writeDescriptorSetBind1.descriptorCount = 1;
	writeDescriptorSetBind1.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	writeDescriptorSetBind1.dstBinding = 0;
	writeDescriptorSetBind1.dstArrayElement = 0;
	writeDescriptorSetBind1.pBufferInfo = &bufferInfo;


	VkWriteDescriptorSet writeDescriptorSetBind2{};
	writeDescriptorSetBind2.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	writeDescriptorSetBind2.dstSet = skyBoxDescriptor;
	writeDescriptorSetBind2.descriptorCount = 1;
	writeDescriptorSetBind2.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	writeDescriptorSetBind2.dstBinding = 1;
	writeDescriptorSetBind2.dstArrayElement = 0;
	writeDescriptorSetBind2.pImageInfo = &imageInfo;

	VkWriteDescriptorSet writes[]{writeDescriptorSetBind1, writeDescriptorSetBind2};

	vkUpdateDescriptorSets(logicalDevice, 2, writes, 0, nullptr);

}
