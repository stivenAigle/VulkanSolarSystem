//
// Created by stiven on 17-12-01.
//

#include <scene.hpp>
#include "descriptorSetLayout.hpp"
#include "logicaldevice.hpp"

DescriptorSetLayout::DescriptorSetLayout(LogicalDevice &logicalDevice):logicalDevice(logicalDevice) {

	VkDescriptorSetLayoutBinding uboLayoutBinding{};
	uboLayoutBinding.binding = 0;
	uboLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	uboLayoutBinding.descriptorCount = Scene::numberPlanet;
	uboLayoutBinding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
	uboLayoutBinding.pImmutableSamplers = nullptr;


	VkDescriptorSetLayoutBinding uboPosLayoutBinding{};
	uboPosLayoutBinding.binding = 1;
	uboPosLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	uboPosLayoutBinding.descriptorCount = Scene::numberPlanet;
	uboPosLayoutBinding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
	uboPosLayoutBinding.pImmutableSamplers = nullptr;

	VkDescriptorSetLayoutBinding ubotexture{};
	ubotexture.binding = 2;
	ubotexture.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	ubotexture.descriptorCount = Scene::numberPlanet;
	ubotexture.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
	ubotexture.pImmutableSamplers = nullptr;

	VkDescriptorSetLayoutBinding uboMap{};
	uboMap.binding = 3;
	uboMap.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	uboMap.descriptorCount = 1;
	uboMap.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
	uboMap.pImmutableSamplers = nullptr;

	VkDescriptorSetLayoutBinding uboObject{};
	uboObject.binding = 4;
	uboObject.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	uboObject.descriptorCount = 1;
	uboObject.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
	uboObject.pImmutableSamplers = nullptr;


	std::array<VkDescriptorSetLayoutBinding, 5> array{uboLayoutBinding, uboPosLayoutBinding, ubotexture, uboMap, uboObject};

	VkDescriptorSetLayoutCreateInfo setLayoutCreateInfo{};
	setLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	setLayoutCreateInfo.bindingCount = array.size();
	setLayoutCreateInfo.pBindings = array.data();

	if(vkCreateDescriptorSetLayout(logicalDevice, &setLayoutCreateInfo, nullptr, &descriptorSetLayout)){
		throw std::runtime_error("unable to create descriptor set layout");
	}

	createLayoutCam();

}

DescriptorSetLayout::~DescriptorSetLayout() {
	vkDestroyDescriptorSetLayout(logicalDevice, descriptorSetLayout, nullptr);
	vkDestroyDescriptorSetLayout(logicalDevice, descriptorSetLayoutCam, nullptr);
}

void DescriptorSetLayout::createLayoutCam() {

	VkDescriptorSetLayoutBinding binding{};
	binding.binding = 0;
	binding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	binding.descriptorCount = 1;
	binding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
	binding.pImmutableSamplers = nullptr;

	VkDescriptorSetLayoutBinding uboCubeMap{};
	uboCubeMap.binding = 1;
	uboCubeMap.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	uboCubeMap.descriptorCount = 1;
	uboCubeMap.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
	uboCubeMap.pImmutableSamplers = nullptr;

	std::array< VkDescriptorSetLayoutBinding, 2> bindings{binding, uboCubeMap};
	VkDescriptorSetLayoutCreateInfo createInfo{};
	createInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	createInfo.bindingCount = bindings.size();
	createInfo.pBindings = bindings.data();

	if(vkCreateDescriptorSetLayout(logicalDevice, &createInfo, nullptr, &descriptorSetLayoutCam)){
		throw std::runtime_error("unable to create descriptor set layout");
	}

}
