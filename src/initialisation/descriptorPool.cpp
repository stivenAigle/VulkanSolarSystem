//
// Created by stiven on 17-12-01.
//

#include <scene.hpp>
#include "descriptorPool.hpp"

DescriptorPool::DescriptorPool(LogicalDevice &logicalDevice) : logicalDevice(logicalDevice) {

	VkDescriptorPoolSize sizePool{};
	sizePool.descriptorCount = Scene::numberPlanet*2 + 2;
	sizePool.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;

	VkDescriptorPoolSize poolImage{};
	poolImage.descriptorCount = Scene::numberPlanet + 2;
	poolImage.type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;

	std::array<VkDescriptorPoolSize, 2> poolSizes{sizePool, poolImage};
	VkDescriptorPoolCreateInfo createInfo{};
	createInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	createInfo.maxSets = sizePool.descriptorCount + poolImage.descriptorCount ;
	createInfo.poolSizeCount = poolSizes.size();
	createInfo.pPoolSizes = poolSizes.data();

	if(vkCreateDescriptorPool(logicalDevice, &createInfo, nullptr, &descriptorPool)){
		throw std::runtime_error("failed to create descriptorPool");
	}

}

DescriptorPool::~DescriptorPool() {
	vkDestroyDescriptorPool(logicalDevice, descriptorPool, nullptr);
}
