//
// Created by stiven on 17-11-20.
//

#include "window.hpp"
#include <GLFW/glfw3.h>
#include <iostream>

Window::Window() {
	if(!glfwInit()){
		std::cerr << "error while init glfw "<< std::endl;
	}
	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
	glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);
	window = glfwCreateWindow(width, heigth, "SolarSystem", nullptr, nullptr);
}

Window::~Window() {

	glfwDestroyWindow(window);
	glfwTerminate();
}

int Window::getWidth() {
	return width;
}

int Window::getHeigth() {
	return heigth;
}
