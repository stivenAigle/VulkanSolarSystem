//
// Created by stiven on 17-11-20.
//

#ifndef SOLARSYSTEM_INSTANCE_HPP
#define SOLARSYSTEM_INSTANCE_HPP

#include <vulkan/vulkan.h>
#include <array>
extern const std::array<const char *, 1> validationLayers;
class Instance {
	VkInstance instance;
	VkDebugReportCallbackEXT callback;


	void setupDebugCallback();
public:

#if NDEBUG
	static constexpr bool enableValidationLayer = false;
#else
	static constexpr bool enableValidationLayer = true;
#endif

	Instance();
	operator VkInstance (){ return instance;}
	~Instance();
};


#endif //SOLARSYSTEM_INSTANCE_HPP
