//
// Created by stiven on 17-11-20.
//

#include <vector>
#include <set>
#include <iostream>
#include "logicaldevice.hpp"

LogicalDevice::LogicalDevice(PhysicalDevice &physicalDevice) : physicalDevice(physicalDevice) {

	std::vector<VkDeviceQueueCreateInfo> queueCreateInfos;
	std::set<int> uniqueQueueFamilies = {physicalDevice.getGraphicsQueue(),  physicalDevice.getPresentQueue()};

	float queuePriority = 1.0f;
	for (int queueFamily : uniqueQueueFamilies) {
		VkDeviceQueueCreateInfo queueCreateInfo = {};
		queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		queueCreateInfo.queueFamilyIndex = queueFamily;
		queueCreateInfo.queueCount = 1;
		queueCreateInfo.pQueuePriorities = &queuePriority;
		queueCreateInfos.push_back(queueCreateInfo);
	}

	VkPhysicalDeviceFeatures deviceFeatures{};
	deviceFeatures.samplerAnisotropy = VK_TRUE;

	VkDeviceCreateInfo deviceCreateInfo{};
	deviceCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	deviceCreateInfo.queueCreateInfoCount = static_cast<uint32_t>(queueCreateInfos.size());
	deviceCreateInfo.pQueueCreateInfos = queueCreateInfos.data() ;

	deviceCreateInfo.pEnabledFeatures = &deviceFeatures;
	deviceCreateInfo.enabledExtensionCount = static_cast<uint32_t>(deviceExtensions.size());
	deviceCreateInfo.ppEnabledExtensionNames = deviceExtensions.data();
#ifndef NDEBUG

		deviceCreateInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
		deviceCreateInfo.ppEnabledLayerNames = validationLayers.data();
#else
		createInfo.enabledLayerCount = 0;
#endif

	if(vkCreateDevice(physicalDevice, &deviceCreateInfo, nullptr, &device)){
		throw std::runtime_error("failed to create logical device!");
	}

	vkGetDeviceQueue(device, static_cast<uint32_t>(physicalDevice.getGraphicsQueue()), 0, &graphicsQueue);
	vkGetDeviceQueue(device, static_cast<uint32_t>(physicalDevice.getPresentQueue()), 0, &presentQueue);

}

LogicalDevice::~LogicalDevice() {
}

 VkQueue LogicalDevice::getGraphicsQueue()  {
	return graphicsQueue;
}

VkQueue LogicalDevice::getPresentQueue()  {
	return presentQueue;
}

void LogicalDevice::destroy() {
	vkDestroyDevice(device, nullptr);

}
