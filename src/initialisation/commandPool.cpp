//
// Created by stiven on 17-11-21.
//

#include "commandPool.hpp"

CommandPool::~CommandPool() {
	vkDestroyCommandPool(logicalDevice, commandPool, nullptr);
}

CommandPool::CommandPool(PhysicalDevice &physicalDevice, LogicalDevice &logicalDevice) : physicalDevice(physicalDevice),
                                                                                         logicalDevice(logicalDevice) {
	const queueFamilyIndices &queueFamilyIndices = physicalDevice.getQueueFamilyIndices();

	VkCommandPoolCreateInfo poolInfo = {};
	poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	poolInfo.queueFamilyIndex = queueFamilyIndices.graphicsFamily;
	poolInfo.flags = 0; // Optional
	if (vkCreateCommandPool(logicalDevice, &poolInfo, nullptr, &commandPool) != VK_SUCCESS) {
		throw std::runtime_error("failed to create command pool!");
	}
}
