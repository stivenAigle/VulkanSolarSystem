//
// Created by stiven on 17-11-21.
//

#ifndef SOLARSYSTEM_FRAMEBUFFERS_HPP
#define SOLARSYSTEM_FRAMEBUFFERS_HPP

#include <vector>
#include "swapChain.hpp"
#include "renderPasses.hpp"
#include "depthImage.hpp"

class FrameBuffers {

	std::vector<VkFramebuffer > swapChainFrambuffer;

	LogicalDevice & logicalDevice;
	SwapChain & swapChain;

	RenderPasses & renderPasses;
	DepthImage &depthImage;
public:
	FrameBuffers(LogicalDevice &logicalDevice, SwapChain &swapChain, RenderPasses &renderPasses,
		             DepthImage &depthImage);

	VkFramebuffer operator[](int i){
		return  swapChainFrambuffer[i]; }
	~FrameBuffers();
};


#endif //SOLARSYSTEM_FRAMEBUFFERS_HPP
