//
// Created by stiven on 17-11-21.
//

#include <stdexcept>
#include <utils/image.hpp>
#include "depthImage.hpp"
#include "physicalDevice.hpp"

VkFormat DepthImage::findSupportedFormat(std::vector<VkFormat> const &candidatees, VkImageTiling tiling,
                                         VkFormatFeatureFlags features, PhysicalDevice &physicalDevice) {

	for(VkFormat format : candidatees){
		VkFormatProperties props;
		vkGetPhysicalDeviceFormatProperties(physicalDevice, format, &props);
		if (tiling == VK_IMAGE_TILING_LINEAR && (props.linearTilingFeatures & features) == features) {
			return format;
		} else if (tiling == VK_IMAGE_TILING_OPTIMAL && (props.optimalTilingFeatures & features) == features) {
			return format;
		}
	}

	throw std::runtime_error("failed to find supported format!");
}
VkFormat DepthImage::findDepthFormat(PhysicalDevice &physicalDevice) {
	return findSupportedFormat({ VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT},
	                           VK_IMAGE_TILING_OPTIMAL, VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT, physicalDevice);
}

DepthImage::DepthImage(LogicalDevice &logicalDevice, PhysicalDevice &physicalDevice, CommandPool &commandPool,
                       SwapChain &swapChain) : logicalDevice(logicalDevice), physicalDevice(physicalDevice),
                                               commandPool(commandPool), swapChain(swapChain) {


	VkFormat depthFormat = findDepthFormat(physicalDevice);

	createImage(swapChain.getSwapChainExtent().width, swapChain.getSwapChainExtent().height, depthFormat, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, depthImage, depthImageMemory, logicalDevice, physicalDevice);
	depthimageView = createImageView(depthImage, depthFormat, VK_IMAGE_ASPECT_DEPTH_BIT |VK_IMAGE_ASPECT_STENCIL_BIT, logicalDevice, 1);

	transitionImageLayout(depthImage, depthFormat, VK_IMAGE_LAYOUT_UNDEFINED,
	                      VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
	                      commandPool, logicalDevice, 1);
}

DepthImage::~DepthImage() {

	vkDestroyImageView(logicalDevice, depthimageView, nullptr);
	vkDestroyImage(logicalDevice, depthImage, nullptr);
	vkFreeMemory(logicalDevice, depthImageMemory, nullptr);

}
