//
// Created by stiven on 17-11-20.
//

#ifndef SOLARSYSTEM_SCENE_HPP
#define SOLARSYSTEM_SCENE_HPP

#include <GLFW/glfw3.h>
#include <array>
#include <vector>
#include <uniformBuffer/position.hpp>
#include "uniformBuffer/transfom.hpp"
#include "Camera.h"
#include "Sun.h"
#include "Planet.h"
constexpr int number = 5;
using uniformArray = std::array<transform, number>;
using posArray = std::array<pos, number>;
class Scene{
public:
	static constexpr unsigned numberPlanet = number;
private:
	Camera cam;
	Sun sun;
	std::vector<Planet> planets;
	uniformArray model;
	posArray positions;
public:

	Scene();
	uniformArray const getTranform() const ;
	posArray const & getPos();
	void move(glm::vec3 const & movement);
	void rotate(glm::vec3 const & angle);
	void rotateKeepLooking(glm::vec3 const & angle);
	void update(float delta);
};
#endif //SOLARSYSTEM_SCENE_HPP
