//
// Created by stiven on 17-12-03.
//

#ifndef SOLARSYSTEM_POSITION_HPP
#define SOLARSYSTEM_POSITION_HPP

#include <glm/vec3.hpp>

struct pos{
	glm::vec3 pos1;
	float size;
};

#endif //SOLARSYSTEM_POSITION_HPP
