//
// Created by stiven on 17-12-12.
//

#ifndef SOLARSYSTEM_UNIFORM_HPP
#define SOLARSYSTEM_UNIFORM_HPP

#include <vulkan/vulkan.h>
#include <initialisation/physicalDevice.hpp>
#include <initialisation/logicaldevice.hpp>
#include <cstring>
#include <utils/buffer.hpp>

template<class T>
class Uniform{
	VkBuffer buffer;
	VkDeviceMemory memory;
	LogicalDevice & logicalDevice;
	PhysicalDevice & physicalDevice;
public:


	static constexpr VkDeviceSize size = sizeof(T);
	Uniform(LogicalDevice &logicalDevice, PhysicalDevice &physicalDevice) : logicalDevice(logicalDevice),
	                                                                         physicalDevice(physicalDevice) {
		createBuffer(size, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
		             VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, buffer, memory,
		             logicalDevice, physicalDevice);
	}

	void update(T const & newThing){
		void * data;
		vkMapMemory(logicalDevice, memory, 0, size, 0, &data);
		memcpy(data, &newThing, size);
		vkUnmapMemory(logicalDevice, memory);
	}

	operator VkBuffer(){ return buffer;}
	~Uniform(){
		vkDestroyBuffer(logicalDevice, buffer, nullptr);
		vkFreeMemory(logicalDevice, memory, nullptr);
	}


};
#endif //SOLARSYSTEM_UNIFORM_HPP
