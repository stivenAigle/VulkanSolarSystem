//
// Created by lysilia on 19/11/17.
//

#include "Sun.h"

#ifdef WIN32
#define M_PI 3.14159
#endif

Sun::Sun(const std::string &name, const std::string &model, float size, const glm::vec3 &pos) :
        name(name), model(model), size(size), pos(pos), updatePosition(pos){

}


glm::mat4 Sun::update(float deltaTime){

    // matrice de rotation sur elle-meme
    glm::mat4 rotYown = glm::rotate(glm::mat4(), ((float)(2* M_PI) / 15.0f) * deltaTime, glm::vec3(0.0, 1.0, 0.0));

    // matrice de scale
    glm::mat4 scale = glm::scale(glm::vec3(size));
    return rotYown*scale ;
}

const std::string &Sun::getModel() const {
    return model;
}

const std::string &Sun::getName()const{
    return name;
}

const glm::vec3 &Sun::getPos() const {
    return pos;
}

const glm::vec3 &Sun::getUpdatePosition() const {
    return updatePosition;
}

float Sun::getSize() const {
    return size;
}

