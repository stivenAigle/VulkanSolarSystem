//
// Created by stiven on 17-11-22.
//

#ifndef SOLARSYSTEM_BUFFER_HPP
#define SOLARSYSTEM_BUFFER_HPP


#include <vulkan/vulkan.h>
#include <initialisation/logicaldevice.hpp>
#include "image.hpp"

void createBuffer(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties, VkBuffer &buffer,
                  VkDeviceMemory &bufferMemory, LogicalDevice &device, PhysicalDevice &physicalDevice);

void copyBuffer(VkBuffer src, VkBuffer dest, VkDeviceSize, LogicalDevice & logicalDevice, CommandPool & pool);
#endif //SOLARSYSTEM_BUFFER_HPP
