//
// Created by stiven on 17-11-21.
//

#ifndef SOLARSYSTEM_IMAGE_HPP
#define SOLARSYSTEM_IMAGE_HPP

#include <vulkan/vulkan.h>
#include <stdexcept>
#include <initialisation/logicaldevice.hpp>
#include <initialisation/commandPool.hpp>

uint32_t findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties, PhysicalDevice &physicalDevice) ;


void createImage(int texWidth, int texHeight, VkFormat format, VkImageTiling tiling, VkImageUsageFlags usage,
                 VkMemoryPropertyFlags properties, VkImage &image, VkDeviceMemory &imageMemory, LogicalDevice &device,
                 PhysicalDevice &physicalDevice);


VkImageView
createImageView(VkImage image, VkFormat format, VkImageAspectFlags aspectFlags, LogicalDevice &device, int layerCount);

void transitionImageLayout(VkImage image, VkFormat format, VkImageLayout oldLayout, VkImageLayout newLayout,
                           CommandPool &commandPool, LogicalDevice &logicalDevice, int layerCount);

VkCommandBuffer beginSingleTimeCommands(LogicalDevice &device, CommandPool &commandPool);

void copyBufferToImage(CommandPool &pool, LogicalDevice &logicalDevice, VkBuffer buffer, VkImage imagee, uint32_t width,
                       uint32_t height, int layer);

void endSingleTimeCommands(VkCommandBuffer commandBuffer, LogicalDevice &device, CommandPool &commandPool);
#endif //SOLARSYSTEM_IMAGE_HPP
