//
// Created by stiven on 17-11-20.
//

#include <GLFW/glfw3.h>
#include <iostream>
#include <thread>
#include "loop.hpp"
using namespace std::chrono_literals;
void input(GLFWwindow *window, int key, int scancode, int action, int mods) {
	Loop * loop = static_cast<Loop *>(glfwGetWindowUserPointer(window));

	if(key == GLFW_KEY_ESCAPE){
		glfwSetWindowShouldClose(window, GLFW_TRUE);
	}else{
		loop->moveCamera(key);
	}

}

void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
	Loop * loop = static_cast<Loop *>(glfwGetWindowUserPointer(window));

	loop->mouseClick(window, button, action, mods);
}

static void cursor_position_callback(GLFWwindow* window, double xpos, double ypos)
{
	Loop * loop = static_cast<Loop *>(glfwGetWindowUserPointer(window));
	loop->mouseMove(window, xpos, ypos);


}
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
	Loop * loop = static_cast<Loop *>(glfwGetWindowUserPointer(window));
	loop->zoom(window, xoffset, yoffset);
}
Loop::Loop(Scene &scene) : scene(scene){
	glfwSetWindowUserPointer(init.getWindow(), this);
	glfwSetKeyCallback(init.getWindow(), input);
	glfwSetCursorPosCallback(init.getWindow(), cursor_position_callback);
	glfwSetMouseButtonCallback(init.getWindow(), mouse_button_callback);
	glfwSetScrollCallback(init.getWindow(), scroll_callback);
	createSemaphore();
}

void Loop::loop() {
	float day = 1;
	float time = day/60.0f;
	float timePassed = 0;
	while(!glfwWindowShouldClose(init.getWindow())){
		glfwPollEvents();
		scene.update(timePassed);
		init.update(scene.getTranform(), scene.getPos());
		draw();

//		glfwWaitEventsTimeout(0.017);
		std::this_thread::sleep_for(17ms);
		timePassed += time;
	}
	vkDeviceWaitIdle(init.getLogicalDevice());
}

Loop::~Loop() {
	vkDestroySemaphore(init.getLogicalDevice(), imageAvailableSemaphore, nullptr);
	vkDestroySemaphore(init.getLogicalDevice(), renderFinishedSemaphore, nullptr);
}

void Loop::draw() {
	uint32_t imageIndex;

	vkAcquireNextImageKHR(init.getLogicalDevice(), (VkSwapchainKHR)init.getSwapChain(), std::numeric_limits<uint64_t>::max(), imageAvailableSemaphore, VK_NULL_HANDLE, &imageIndex);

	VkSubmitInfo submitInfo{};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitInfo.commandBufferCount = 1;
	VkCommandBuffer a = init.getCommandBuffers()[imageIndex];
	submitInfo.pCommandBuffers = &a;

	VkPipelineStageFlags waitStage[]={VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
	submitInfo	.waitSemaphoreCount = 1;
	submitInfo.pWaitSemaphores = &imageAvailableSemaphore;
	submitInfo	.pWaitDstStageMask = waitStage;
	submitInfo.signalSemaphoreCount = 1;
	submitInfo.pSignalSemaphores = &renderFinishedSemaphore;

	if (vkQueueSubmit(init.getLogicalDevice().getGraphicsQueue(), 1, &submitInfo, VK_NULL_HANDLE) != VK_SUCCESS) {
		throw std::runtime_error("failed to submit draw command planetBuffer!");
	}

	VkSubpassDependency dependency = {};
	dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
	dependency.dstSubpass = 0;
	dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependency.srcAccessMask = 0;
	dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;



	VkSwapchainKHR swapChains[] = {init.getSwapChain()};
	VkPresentInfoKHR presentInfo{};
	presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
	presentInfo.waitSemaphoreCount = 1;
	presentInfo.pWaitSemaphores = &renderFinishedSemaphore;
	presentInfo.swapchainCount = 1;
	presentInfo.pImageIndices = &imageIndex;
	presentInfo.pSwapchains = swapChains;
	presentInfo.pNext = nullptr;

	vkQueuePresentKHR(init.getLogicalDevice().getPresentQueue(), &presentInfo);

}

void Loop::createSemaphore() {
	VkSemaphoreCreateInfo createInfo{};
	createInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
	createInfo.pNext = nullptr;
	createInfo.flags = 0;
	if(vkCreateSemaphore(init.getLogicalDevice(), &createInfo, nullptr, &imageAvailableSemaphore) || vkCreateSemaphore(init.getLogicalDevice(), &createInfo, nullptr, &renderFinishedSemaphore)){
		throw std::runtime_error("failed create semaphore");
	}
}

void Loop::moveCamera(int key) {

	if(key == GLFW_KEY_W){
		scene.move({0, 0, move});
	}else if(key == GLFW_KEY_S){
		scene.move({0, 0, -move});
	}else if(key == GLFW_KEY_A){
		scene.move({-move, 0, 0});
	}else if(key == GLFW_KEY_D){
		scene.move({move, 0, 0});
	}else if(key == GLFW_KEY_RIGHT){
		scene.rotate({0, angle, 0});
	}else if(key == GLFW_KEY_LEFT) {
		scene.rotate({0, -angle, 0});
	}else if(key == GLFW_KEY_UP){
		scene.rotateKeepLooking({-angle, 0, 0});
	}else if(key == GLFW_KEY_DOWN) {
		scene.rotateKeepLooking({angle, 0, 0});
	}

}

void Loop::mouseClick(GLFWwindow *window, int button, int action, int mods) {
	static double xpos, ypos;
//	static float move = 0.02f;

	if((button == GLFW_MOUSE_BUTTON_MIDDLE && action == GLFW_PRESS )){
		double x, y;
		pressed = true;
		glfwGetCursorPos(window, &x, &y);
		if(y > ypos){
		}
		glfwGetCursorPos(window, &xpos, &ypos);

	}else if(button == GLFW_MOUSE_BUTTON_MIDDLE && action == GLFW_RELEASE ){

		pressed = false;
	}

}

bool isSuperior(double a, double b, double delta){
	return a > b + delta ;
}
void Loop::mouseMove(GLFWwindow *window, double x, double y) {
	static double xpos, ypos;
//	static float move = 0.02f;
	static double delta = 2;
	if(pressed){

		if(isSuperior(y, ypos, delta)){
			scene.rotateKeepLooking({angle, 0, 0});

		}else if (isSuperior(ypos, y, delta)){
			scene.rotateKeepLooking({-angle, 0, 0});
		}
		if(isSuperior(x, xpos, delta)){
			scene.rotateKeepLooking({0, -angle, 0});

		}else if ( isSuperior(xpos, x, delta)){
			scene.rotateKeepLooking({0, angle, 0});
		}
		xpos = x;
		ypos = y;

	}

}

void Loop::zoom(GLFWwindow *window, double xoffset, double yoffset) {
	if(yoffset > 0){
		scene.move({0, 0, move});

	}else if (yoffset < 0){
		scene.move({0, 0, -move});
	}

}


