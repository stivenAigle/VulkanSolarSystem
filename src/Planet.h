//
// Created by lysilia on 18/11/17.
//

#ifndef VALKYRIA_PLANET_H
#define VALKYRIA_PLANET_H

#include <string>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include "Sun.h"

class Planet : public Sun{

private:
    float dayTime;
    float yearTime;
    float ownVelocity;
    float orbitalVelocity;
    bool tutu;

    const Sun& origine;

public:
    Planet(const std::string &name, const std::string &model, float size, const glm::vec3 &pos, float dayTime, float yearTime,
           const Sun &origine);

    glm::mat4 update(float deltaTime) override;

    bool operator<(const Planet& bob);


};

inline bool operator<(const Planet& bob1, const Planet& bob2){
    return bob1.getName() < bob2.getName();
}


#endif //VALKYRIA_PLANET_H
