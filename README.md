# Solar System
It is written in C++ and use Vulkan API for 3D rendering. 
The window system is handle by GLFW3.3
It's not the real Solar System but it purpose is to be a sand box for my experiment. 

Tested on Linux Mint 18 and Windows 10 
## Compiling
For compiling you need the following things :
* glm Library
* a C++17 compatible compiler (tested on g++-7 and MSVC 2017)
* CMake 3.0 at least

## Movement
You can move in this system by using the **w, a, s, d** keys. You can also use the **wheel of your mouse** to go forward and backward. 
Finally, you can rotate the camera by clicking on your **middle mouse button** and move your mouse while holding.

## Features tested
* Shadow on a plane
* Shadow by RayCasting 
* Mirror effect with a Stencil Buffer
* Adding Fog
* SkyBox 
* Phong lightning model
* Reflection of a SkyBox over a sphere