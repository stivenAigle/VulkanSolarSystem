//
// Created by stiven on 17-10-08.
//

#ifndef GLENGINE_UTILS_H
#define GLENGINE_UTILS_H

#include <optional>
namespace glEngine{
    struct material {

        std::optional<std::string> diffuseTexturePath;
        std::optional<std::string> ambientTexturePath;
        std::optional<std::string> specularTexturePath;
        std::optional<int> first;
        std::optional<int> count;
        std::optional<glm::vec3> ambient;
        std::optional<glm::vec3> diffuse;
        std::optional<glm::vec3> specular;
        std::optional<float> exposant;
        std::optional<float> shininess;

    };
     using mapMaterials = std::map<std::string, material>;




}
#endif //GLENGINE_UTILS_H
