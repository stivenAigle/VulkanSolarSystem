//
// Created by stiven on 17-10-16.
//

#ifndef GLENGINE_PROGRAMMANAGE_H
#define GLENGINE_PROGRAMMANAGE_H
// TODO: class provisoire permettant juste d'afficher avec une lumiere diffuse
#include <glm/detail/type_mat.hpp>
#include <glish/utils/ProgramGL.hpp>
#include <glEngine/scene/node.h>

namespace glEngine {
    enum class uni{
        cam, transfo, image
    };
	enum class sky{
		proj, view, cubeMap
	};
    class Camera;
    class Node;
    class ProgramManager {

        using myProgram  = glish::ProgramGL<uni, glm::mat4, glm::mat4, int>;
        using skyBoxProgram = glish::ProgramGL<sky, glm::mat4, glm::mat4, int>;
        static ProgramManager programManager;
        myProgram prog;
	    skyBoxProgram skyBoxProg;

    public:
        ProgramManager();
        static void uniCam(Camera const & cam);
        static void uniMovable(const Node *node);
        static void init();
	    static void useSky();
	    static void useProg();
	    static void useCubeMap(Camera const &cam);


    };
}
#endif //GLENGINE_PROGRAMMANAGE_H
