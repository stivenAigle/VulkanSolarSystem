//
// Created by stiven on 17-11-26.
//

#ifndef ALLPROJECT_GLENGINE_HPP
#define ALLPROJECT_GLENGINE_HPP

#include <string>
namespace glEngine {

	void init();
	void addSkyBox(std::string const &  name, std::string const & extension);
}
#endif //ALLPROJECT_GLENGINE_HPP
