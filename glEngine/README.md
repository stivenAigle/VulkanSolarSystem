#glEngine

3D graphics engine in c++17 and OpenGL 4.0

##compile 
You need :

        gcc 7.0 
        cmake 3.7
        SDL2
        SDL2_image
        
##Dependencies:

    glish :
    OpenGL Wrapper developed in C++ 17 to simplify the use of OpenGL API

    SDL2 :
    Use to create the window context and manage input

    glm :
    Mathematics Library design to be use with OpenGL